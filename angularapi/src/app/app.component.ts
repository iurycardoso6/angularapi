import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { take } from "rxjs/operators";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "angularapi";

  API = "http://idcicdeb-env.eba-mqfx2tnz.us-east-1.elasticbeanstalk.com/User";

  user = {
    PersonalData: {
      Name: "Cauê Marcelo Caldeira",
      Email: "caldeira@coldblock.com.br",
      Document: "25185583738",
      rg: "334048011",
      Password: "PWenw7z1dfbVFofl+aA31A==",
      Provider: null,
      SocialUserId: "",
    },
    Address: {
      ibgeCode: 4312401,
      cep: 95780000,
      address: "rua tres",
      number: 244,
      district: "cinco",
      complement: "nove",
    },
    NotificationOption: {
      sms: true,
      email: false,
      whatsapp: false,
    },
  };

  constructor(private http: HttpClient) {}

  ngOnInit() {}

  criarUsuario(usuario) {
    return this.http.post(this.API, usuario).pipe(take(1));
  }

  submitt() {
    this.criarUsuario(this.user).subscribe(
      (success) => console.log("AppComponent -> submitt -> success", success),
      (error) => console.log("AppComponent -> submitt -> error", error),
      () => console.log("request OK")
    );
  }
}
